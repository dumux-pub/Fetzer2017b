// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/**
 * \file
 * \brief This file contains functions which are used to convert relative humidity
 *        to mass fractions.
 *
 * The algorithm is taken from http://www.cactus2000.de.
 * The function for calculating the water vapor pressure is:<br>
 * Lowe, P. R. and J. M. Ficke, 1974: "The computation of saturation vapor pressure"
 *     Tech. Paper No. 4-74, Environmental Prediction Research Facility,
 *     Naval Postgraduate School, Monterey, CA, 27pp
 * \note At least the spurious checked data was in good agreement with the
 *       Mollier-diagram.
 */
#ifndef DUMUX_H2O_AIR_RELATIVE_HUMIDITY_CONVERTER
#define DUMUX_H2O_AIR_RELATIVE_HUMIDITY_CONVERTER

#include<cmath>
#include<iostream>

namespace Dumux
{

double convertRelativeHumudityToMassFraction(double temperatureKelvin, double pressure,
                                             double relativeHumidity)
{
    double temperatureCelsius = temperatureKelvin - 273.15;
    double waterVaporPressure = 0.0;
    waterVaporPressure = (6.107799961
                          + temperatureCelsius * (4.436518521e-1
                            + temperatureCelsius * (1.428945805e-2
                              + temperatureCelsius * (2.650648471e-4
                                + temperatureCelsius * (3.031240396e-6
                                  + temperatureCelsius * (2.034080948e-8
                                    + temperatureCelsius * 6.136820929e-11))))))
                          * 100.0 /*convert to Pa*/
                          * relativeHumidity;

    double molarMassWater = 18.01534; // g/mol
    double molarMassDryAir = 28.9644; // g/mol
    double partialPressureRatio = waterVaporPressure / pressure;
    double specificHumidity = partialPressureRatio * molarMassWater
                              / (partialPressureRatio * molarMassWater
                                  + (1.0 - partialPressureRatio) * molarMassDryAir);
//     std::cout << "T[°C]: " << temperatureCelsius
//               << " p[Pa]: " << pressure
//               << " RH[-]: " << relativeHumidity
//               << " pw[Pa]: " << waterVaporPressure
//               << " x[mol/mol]: " << partialPressureRatio
//               << " SH [kg/kg]: " << specificHumidity
//               << std::endl;
    return specificHumidity;
}

} // namespace Dumux

#endif // DUMUX_H2O_AIR_RELATIVE_HUMIDITY_CONVERTER