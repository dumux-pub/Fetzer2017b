// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A simple dgf writer, which creates a dgf for the given domain
 */

#ifndef DUMUX_DGF_WRITER_HH
#define DUMUX_DGF_WRITER_HH

#include <fstream>

namespace Dumux
{
/*!
 * \brief A simple dgf writer, which creates a dgf for the given domain
 */

template <class Grid>
class DGFWriter
{
public:
    enum {dim = Grid::dimension};
    typedef typename Grid::ctype Scalar;

    void write(const std::string& dgfName,
               const Dune::FieldVector<Scalar, dim>& domainMin,
               const Dune::FieldVector<Scalar, dim>& domainMax,
               const Dune::FieldVector<int, dim>& cells)
    {
        std::ofstream outfile;
        outfile.open(dgfName);
        outfile << "DGF" << std::endl;
        outfile << "Interval" << std::endl;
        outfile << domainMin[0] << " " << domainMin[1] << " % first corner" << std::endl;
        outfile << domainMax[0] << " " << domainMax[1] << " % second corner" << std::endl;
        outfile << cells[0] << " " << cells[1] << " % cells in x and y direction" << std::endl;
        outfile << "#" << std::endl;
        outfile << "" << std::endl;
        outfile << "Cube" << std::endl;
        outfile << "0 1 2 3" << std::endl;
        outfile << "#" << std::endl;
        outfile << "" << std::endl;
        outfile << "BOUNDARYDOMAIN" << std::endl;
        outfile << "default 1    % all boundaries have id 1" << std::endl;
        outfile << "BOUNDARYDOMAIN" << std::endl;
        outfile << "# unitcube.dgf" << std::endl;
        outfile.close();
    }
};
}


#endif
