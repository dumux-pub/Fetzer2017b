SUMMARY
=======
This is the DuMuX module containing the code for producing the results
submitted for:

T. Fetzer, J. Vanderborght, K. Mosthaf, K.M. Smits, and R. Helmig<br>
[Heat and water transport in soils and across the soil-atmosphere interface: 2. Numerical analysis]
(http://dx.doi.org/10.1002/2016WR019983)<br>
Water Resources Research, 2017<br>
DOI: 10.1002/2016WR019983

You can use the .bib file provided [here](Fetzer2017b.bib).

Installation
============

The easiest way to install this module is to create a new folder and to execute the file
[installFetzer2017b.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Fetzer2017b/raw/master/installFetzer2017b.sh)
in this folder.

```bash
mkdir -p Fetzer2017b && cd Fetzer2017b
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Fetzer2017b/raw/master/installFetzer2017b.sh
sh ./installFetzer2017b.sh
```

For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.


Applications
============

The applications used for this publication can be found in appl/multidomain/evapcon.
This folder also contains the jobfiles which specify the individual setups.
There is a test which can be run using the ctest facility to check whether the reference
results are obtained.

* __test_evapcon__:
  The basic setup of a wind tunnel coupled to a heterogeneous porous medium below.

In order to run the executable, type:
```bash
cd dumux-Fetzer2017b/build-cmake/appl/multidomain/evapcon/
make
./test_evapcon
```


Used Versions and Software
==========================

For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installFetzer2017b.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Fetzer2017b/raw/master/installFetzer2017b.sh).

In addition the following external software packages, with one of the two linear
solvers, are necessary for compiling the executables:

| software           | version | type              |
| ------------------ | ------- | ----------------- |
| cmake              | 3.5.2   | build tool        |
| suitesparse        | 4.4.6   | matrix algorithms |
| UMFPack            | 5.7.1   | linear solver     |
| SuperLU            | 4.3     | linear solver     |

The module has been checked for the following compilers:

| compiler      | version |
| ------------- | ------- |
| clang/clang++ | 3.5     |
| gcc/g++       | 5.0     |
