#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
builddir=/temp/fetzer/dumux-29/dumux-Fetzer2017b/build-gcc/
outdir=/temp/fetzer/resultsFetzer2017b/

# predefined names
executable=test_evapcon
input=massBL.input
sourcedir=$builddir/appl/multidomain/evapcon
simdir=$outdir/results/massBL/0.1mps/0.2m

# make executable
cd $sourcedir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ]; then
  exit 1
fi
mkdir -p $simdir

cp $sourcedir/$executable $simdir
cp $sourcedir/$input $simdir
cd $simdir

echo "simulation starts on $HOST" | tee logfile.out
COMMAND="./$executable $input \
  -Grid.Cells0 20 \
  -Grid.Positions0 '0.0 0.40' \
  -Grid.NoDarcyX2 0.30 \
  -FreeFlow.RefVelocity 0.1 \
  -TimeManager.DtInitial 1.0 \
  -Newton.RelTolerance 1e-5 \
  -Newton.TargetSteps 3 \
  -Newton.MaxSteps 5 \
  | tee -a logfile.out"
echo $COMMAND > simulation.sh && chmod u+x simulation.sh
./simulation.sh
echo -e "\nsimulation ended on $HOST" | tee -a logfile.out
exit 0
