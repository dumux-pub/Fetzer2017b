#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
builddir=/temp/fetzer/dumux-29/dumux-Fetzer2017b/build-gcc/
outdir=/temp/fetzer/resultsFetzer2017b/

# predefined names
executable=test_evapcon
input=heterogeneity.input
sourcedir=$builddir/appl/multidomain/evapcon
simdir=$outdir/results/heterogeneity/sandSilt_x40

# make executable
cd $sourcedir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ]; then
  exit 1
fi
mkdir -p $simdir

cp $sourcedir/$executable $simdir
cp $sourcedir/$input $simdir
cd $simdir

echo "simulation starts on $HOST" | tee logfile.out
COMMAND="./$executable $input \
  -Grid.Cells0 '10 21 10' \
  -SpatialParams.LeftSoil 2 \
  -SpatialParams.RightSoil 1 \
  | tee -a logfile.out"
echo $COMMAND > simulation.sh && chmod u+x simulation.sh
./simulation.sh
echo -e "\nsimulation ended on $HOST" | tee -a logfile.out
exit 0
