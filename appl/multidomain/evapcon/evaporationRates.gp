reset
set datafile separator ';'

set title 'Evaporation Experiments at Colorado School of Mines (Davarzani2014)'
set xlabel 'Time [d]'
set ylabel 'Evaporation rate [mm/d]'
# set xrange [0:12]
set yrange [0:20]

plot \
'storage.out' u ($1/86400):($7*86400) w l, \
'fluxes.out' u ($1/86400):($2*86400/0.2) w l t 'fluxes - total', \
'fluxes.out' u ($1/86400):($6*86400/0.1) w l t 'fluxes - soil1', \
'fluxes.out' u ($1/86400):($10*86400/0.1) w l t 'fluxes - soil2'

set terminal pngcairo size 800,600
set output 'evaporationRatesMines.png'
replot
