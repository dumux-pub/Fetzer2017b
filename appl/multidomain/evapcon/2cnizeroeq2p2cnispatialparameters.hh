// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Spatial parameters for the
 *        coupling of a non-isothermal two-component ZeroEq
 *        and a non-isothermal two-phase two-component Darcy model.
 */
#ifndef DUMUX_TWOCNIZEROEQTWOPTWOCNISPATIALPARAMS_HH
#define DUMUX_TWOCNIZEROEQTWOPTWOCNISPATIALPARAMS_HH

#include <dune/grid/io/file/vtk/common.hh>

#include <dumux/material/spatialparams/implicit.hh>
// #include <dumux/material/fluidmatrixinteractions/2p/linear.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchten.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearizedregvangenuchtenparams.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>
#include <dumux/io/plotmateriallaw.hh>

namespace Dumux
{

//forward declaration
template<class TypeTag>
class TwoCNIZeroEqTwoPTwoCNISpatialParams;

namespace Properties
{
// The spatial parameters TypeTag
NEW_TYPE_TAG(TwoCNIZeroEqTwoPTwoCNISpatialParams);

// Set the spatial parameters
SET_TYPE_PROP(TwoCNIZeroEqTwoPTwoCNISpatialParams, SpatialParams, TwoCNIZeroEqTwoPTwoCNISpatialParams<TypeTag>);

// Set the material Law
SET_PROP(TwoCNIZeroEqTwoPTwoCNISpatialParams, MaterialLaw)
{
 private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
//     typedef RegularizedBrooksCorey<Scalar> EffMaterialLaw;
    typedef RegularizedVanGenuchten<Scalar> EffMaterialLaw;
//     typedef RegularizedVanGenuchten<Scalar, LinearizedRegVanGenuchtenParams<Scalar, TypeTag> > EffMaterialLaw;
 public:
    typedef EffToAbsLaw<EffMaterialLaw> type;
};
}


/*!
 * \ingroup TwoPTwoCNIModel
 * \ingroup ZeroEqTwoCNIModel
 * \ingroup ImplicitTestProblems
 * \brief Definition of the spatial parameters for
 *        the coupling of a non-isothermal two-component ZeroEq
 *        and a non-isothermal two-phase two-component Darcy model.
 */
template<class TypeTag>
class TwoCNIZeroEqTwoPTwoCNISpatialParams : public ImplicitSpatialParams<TypeTag>
{
    typedef ImplicitSpatialParams<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GridView::ctype CoordScalar;

    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld
    };

    typedef Dune::FieldVector<CoordScalar,dim> LocalPosition;
    typedef Dune::FieldVector<CoordScalar,dimWorld> GlobalPosition;
    typedef Dune::FieldVector<CoordScalar,dimWorld> DimVector;

    typedef typename GridView::IndexSet IndexSet;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::template Codim<0>::Entity Element;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;

    typedef std::vector<Scalar> PermeabilityType;
    typedef typename GET_PROP(TypeTag, ParameterTree) ParameterTree;
    typedef typename GridView::template Codim<dim>::Iterator VertexIterator;

public:
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename MaterialLaw::Params MaterialLawParams;
    typedef std::vector<MaterialLawParams> MaterialLawParamsVector;

    /*!
     * \brief Spatial parameters for the
     *        coupling of a non-isothermal two-component ZeroEq
     *        and a non-isothermal two-phase two-component Darcy model.
     *
     * \param gridView The GridView which is used by the problem
     */
    TwoCNIZeroEqTwoPTwoCNISpatialParams(const GridView& gridView)
        : ParentType(gridView),
          randomPermeability_(gridView.size(dim), 0.0),
          indexSet_(gridView.indexSet()),
          eps_(1e-7)
    {
        heterogeneityAtX_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, HeterogeneityAtX);
        leftSoil_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, SpatialParams, LeftSoil);
        rightSoil_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, SpatialParams, RightSoil);
        alphaBJ_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, AlphaBJ);
        plotMaterialLaw_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, SpatialParams, PlotMaterialLaw);

        permeability_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Permeability);
        porosity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Porosity);
        thermalConductivitySolid_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, ThermalConductivitySolid);

        swr_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Swr);
        snr_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Snr);
        spatialParams_.setSwr(swr_);
        spatialParams_.setSnr(snr_);
        spatialParams_.setVgAlpha(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, VgAlpha));
        spatialParams_.setVgn(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, VgN));
        spatialParams_.setPcLowSw(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, RegularizationLowSw));

        if (leftSoil_ == 2 || rightSoil_ == 2)
        {
            permeability2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Permeability2);
            porosity2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Porosity2);
            thermalConductivitySolid2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, ThermalConductivitySolid2);

            swr2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Swr2);
            snr2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Snr2);
            spatialParams2_.setSwr(swr2_);
            spatialParams2_.setSnr(snr2_);
            spatialParams2_.setVgAlpha(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, VgAlpha2));
            spatialParams2_.setVgn(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, VgN2));
            spatialParams2_.setPcLowSw(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, RegularizationLowSw2));
        }

        Scalar xMax = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX2);
        if (heterogeneityAtX_ < xMax)
        {
            noAdvectiveFluxOverPMInterface_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, SpatialParams, NoAdvectiveFluxOverPMInterface);
            noDiffusiveFluxOverPMInterface_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, SpatialParams, NoDiffusiveFluxOverPMInterface);
            noConductiveFluxOverPMInterface_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, SpatialParams, NoConductiveFluxOverPMInterface);
        }
    }


    /*!
     * \brief Returns the intrinsic permeability tensor \f$[m^2]\f$
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param scvIdx The local index of the sub-control volume
     */
    Scalar intrinsicPermeability(const Element &element,
                                 const FVElementGeometry &fvGeometry,
                                 const int scvIdx) const
    {
        const GlobalPosition &globalPos = element.geometry().corner(scvIdx);
        if (checkSoilType(globalPos) == 1)
            return permeability_;
        else
            return permeability2_;
    }

    /*!
     * \brief Returns the porosity \f$[-]\f$
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param scvIdx The local index of the sub-control volume
     */
    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    const int scvIdx) const
    {
        const GlobalPosition &globalPos = element.geometry().corner(scvIdx);
        if (checkSoilType(globalPos) == 1)
            return porosity_;
        else
            return porosity2_;
    }


    /*!
     * \brief Returns the parameter object for the material law
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param scvIdx The local index of the sub-control volume
     */
    const MaterialLawParams& materialLawParams(const Element &element,
                                               const FVElementGeometry &fvGeometry,
                                               const int scvIdx) const
    {
        const GlobalPosition &globalPos = element.geometry().corner(scvIdx);
        if (checkSoilType(globalPos) == 1)
            return spatialParams_;
        else
            return spatialParams2_;
    }


    /*!
     * \brief Returns the heat capacity \f$[J / (kg K)]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume
     */
    Scalar solidHeatCapacity(const Element &element,
                             const FVElementGeometry &fvGeometry,
                             const int scvIdx) const
    {
        return 790; // specific heat capacity of granite [J / (kg K)]
    }

    /*!
     * \brief Returns the mass density \f$[kg / m^3]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume
     */
    Scalar solidDensity(const Element &element,
                        const FVElementGeometry &fvGeometry,
                        const int scvIdx) const
    {
        return 2650; // density of granite [kg/m^3]
    }

    /*!
     * \brief Returns the thermal conductivity \f$[W/(m*K)]\f$ of the solid
     *
     * This is only required for non-isothermal models.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param scvIdx The local index of the sub-control volume
     */
    Scalar solidThermalConductivity(const Element &element,
                                    const FVElementGeometry &fvGeometry,
                                    const int scvIdx) const
    {
        const GlobalPosition &globalPos = element.geometry().corner(scvIdx);
        if (checkSoilType(globalPos) == 1)
            return thermalConductivitySolid_;
        else
            return thermalConductivitySolid2_;
    }

    /*!
     * \brief Returns the index of the used soil type
     *
     * \param pos The global position
     */
    const unsigned checkSoilType(const GlobalPosition &globalPos) const
    {
        if (globalPos[0] < heterogeneityAtX_ - eps_)
            return leftSoil_;
        else if (globalPos[0] > heterogeneityAtX_ + eps_)
            return rightSoil_;
        else // is on interface, NOTE: necessary for blocking fluxes
            return 3;
    }

    /*!
     * \brief This is called from the coupled problem and creates
     *        a gnuplot output of the Pc-Sw curve
     */
    void plotMaterialLaw()
    {
        if (plotMaterialLaw_)
        {
            PlotMaterialLaw<TypeTag> pcsw;
            if(leftSoil_ == 1 || rightSoil_ == 1)
                pcsw.plotpcsw(spatialParams_, 0.0, 1.0, "pcsw-1");
            if(leftSoil_ == 2 || rightSoil_ == 2)
                pcsw.plotpcsw(spatialParams2_, 0.0, 1.0, "pcsw-2");

            PlotMaterialLaw<TypeTag> krnsw;
            if(leftSoil_ == 1 || rightSoil_ == 1)
                krnsw.plotkr(spatialParams_, 0.0, 1.0, "pcsw-1");
            if(leftSoil_ == 2 || rightSoil_ == 2)
                krnsw.plotkr(spatialParams2_, 0.0, 1.0, "pcsw-2");
        }
    }

    /*!
     * \brief Evaluate the Beavers-Joseph coefficient
     *        at the center of a given intersection
     *
     * \param GlobalPosition global Position
     *
     * \return Beavers-Joseph coefficient
     */
    Scalar beaversJosephCoeffAtPos(const GlobalPosition globalPos) const
    {
        return alphaBJ_;
    }

private:
    Scalar heterogeneityAtX_;
    int leftSoil_;
    int rightSoil_;
    Scalar alphaBJ_;
    bool plotMaterialLaw_;

    Scalar permeability_;
    Scalar porosity_;
    Scalar thermalConductivitySolid_;
    Scalar swr_;
    Scalar snr_;
    MaterialLawParams spatialParams_;

    Scalar permeability2_;
    Scalar porosity2_;
    Scalar thermalConductivitySolid2_;
    Scalar swr2_;
    Scalar snr2_;
    MaterialLawParams spatialParams2_;

    bool noAdvectiveFluxOverPMInterface_;
    bool noDiffusiveFluxOverPMInterface_;
    bool noConductiveFluxOverPMInterface_;

    MaterialLawParamsVector materialLawParams_;
    PermeabilityType randomPermeability_;
    const IndexSet& indexSet_;
    Scalar eps_;
};
} // end namespace

#endif // DUMUX_TWOCNIZEROEQTWOPTWOCNISPATIALPARAMS_HH
