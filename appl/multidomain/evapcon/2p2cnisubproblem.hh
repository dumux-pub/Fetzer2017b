// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Non-isothermal two-phase two-component porous-medium subproblem
 *        with coupling at the top boundary.
 */
#ifndef DUMUX_2P2CNISUB_PROBLEM_HH
#define DUMUX_2P2CNISUB_PROBLEM_HH

#include <dumux/porousmediumflow/2p2c/implicit/indices.hh>
#include <dumux/porousmediumflow/implicit/problem.hh>
#include <dumux/material/fluidmatrixinteractions/2p/thermalconductivityjohansen.hh>
#include <dumux/multidomain/subdomainpropertydefaults.hh>
#include <dumux/multidomain/localoperator.hh>
#include <dumux/multidomain/2cnistokes2p2cni/2p2cnicouplinglocalresidual.hh>

#include "2cnizeroeq2p2cnispatialparameters.hh"

namespace Dumux
{
template <class TypeTag>
class TwoPTwoCNISubProblem;

namespace Properties
{
NEW_TYPE_TAG(TwoPTwoCNISubProblem,
    INHERITS_FROM(BoxTwoPTwoCNI, SubDomain, TwoCNIZeroEqTwoPTwoCNISpatialParams));

// Set the problem property
SET_TYPE_PROP(TwoPTwoCNISubProblem, Problem, TwoPTwoCNISubProblem<TTAG(TwoPTwoCNISubProblem)>);

// Use the 2p2cni local jacobian operator for the 2p2cniCoupling model
SET_TYPE_PROP(TwoPTwoCNISubProblem, LocalResidual, TwoPTwoCNICouplingLocalResidual<TypeTag>);

// Choose pn and Sw as primary variables
SET_INT_PROP(TwoPTwoCNISubProblem, Formulation, TwoPTwoCFormulation::pnsw);

// The gas component balance (air) is replaced by the total mass balance
SET_INT_PROP(TwoPTwoCNISubProblem, ReplaceCompEqIdx, GET_PROP_TYPE(TypeTag, Indices)::contiNEqIdx);

// Used the fluid system from the coupled problem
SET_TYPE_PROP(TwoPTwoCNISubProblem, FluidSystem,
              typename GET_PROP_TYPE(typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag), FluidSystem));

// Johansen is used as model to compute the effective thermal heat conductivity
SET_TYPE_PROP(TwoPTwoCNISubProblem, ThermalConductivityModel,
              ThermalConductivityJohansen<typename GET_PROP_TYPE(TypeTag, Scalar)>);

// Use formulation based on mass fractions
SET_BOOL_PROP(TwoPTwoCNISubProblem, UseMoles, false);

// Use Kelvin equation to account for vapor pressure reduction
SET_BOOL_PROP(TwoPTwoCNISubProblem, UseKelvinEquation, true);

// Enable/disable velocity output
SET_BOOL_PROP(TwoPTwoCNISubProblem, VtkAddVelocity, true);

// Enable gravity
SET_BOOL_PROP(TwoPTwoCNISubProblem, ProblemEnableGravity, true);
}

/*!
 * \ingroup ImplicitTestProblems
 * \ingroup MultidomainProblems
 * \brief Non-isothermal two-phase two-component porous-medium subproblem
 *        with coupling at the top boundary.
 *
 * \todo update description
 *
 * This sub problem uses the \ref TwoPTwoCModel. It is part of the 2p2cni model and
 * is combined with the zeroeq2cnisubproblem for the free flow domain.
 */
template <class TypeTag = TTAG(TwoPTwoCNISubProblem) >
class TwoPTwoCNISubProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GridView::Grid Grid;

    typedef TwoPTwoCNISubProblem<TypeTag> ThisType;
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;

    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    // the type tag of the coupled problem
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag) CoupledTypeTag;

    enum { numEq = GET_PROP_VALUE(TypeTag, NumEq) };
    enum { numPhases = GET_PROP_VALUE(TypeTag, NumPhases) };
    enum { numComponents = GET_PROP_VALUE(TypeTag, NumComponents) };
    enum { // the equation indices
        contiTotalMassIdx = Indices::contiNEqIdx,
        contiWEqIdx = Indices::contiWEqIdx,
        energyEqIdx = Indices::energyEqIdx
    };
    enum { // the indices of the primary variables
        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,
        temperatureIdx = Indices::temperatureIdx
    };
    enum { // the indices for the phase presence
        wCompIdx = Indices::wCompIdx,
        nCompIdx = Indices::nCompIdx
    };
	enum { // the indices for the phase presence
        wPhaseOnly = Indices::wPhaseOnly,
        nPhaseOnly = Indices::nPhaseOnly,
        bothPhases = Indices::bothPhases
    };
    enum {
        wPhaseIdx = Indices::wPhaseIdx,
        nPhaseIdx = Indices::nPhaseIdx
    };
    enum { // grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::Intersection Intersection;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

public:
    /*!
     * \brief The sub-problem for the porous-medium subdomain
     *
     * \param timeManager The TimeManager which is used by the simulation
     * \param gridView The simulation's idea about physical space
     */
    TwoPTwoCNISubProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
    {
        Scalar noDarcyX1 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX1);
        Scalar noDarcyX2 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX2);
        std::vector<Scalar> positions0 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::vector<Scalar>, Grid, Positions0);
        std::vector<Scalar> positions1 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::vector<Scalar>, Grid, Positions1);

        bBoxMin_[0] = std::max(positions0.front(),noDarcyX1);
        bBoxMax_[0] = std::min(positions0.back(),noDarcyX2);
        bBoxMin_[1] = positions1.front();
        bBoxMax_[1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePosY);

        // parts of the interface without coupling
        try { runUpDistanceX1_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, RunUpDistanceX1); }
        catch (...) { runUpDistanceX1_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX1); }
        try { runUpDistanceX2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, RunUpDistanceX2); }
        catch (...) { runUpDistanceX2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX2); }

        refTemperature_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, RefTemperaturePM);
        refPressure_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, RefPressurePM);
        refSw_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, RefSw);
        refSw2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, RefSw2);
        isolatedPorousMediumBox_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, PorousMedium, IsolatedPorousMediumBox);
        heterogeneityAtX_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, HeterogeneityAtX);

        refVelocity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefVelocity);
        freqMassOutput_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Output, FreqMassOutput);

        initializationTime_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, InitTime);

        storageLastTimestep_ = Scalar(0);
        storageChangeLastTimestep_ = Scalar(0);
        lastMassOutputTime_ = Scalar(0);

        outfile.open("storage.out");
        outfile << "Time[s]" << ";"
                << "TotalMassChange[kg/s]" << ";"
                << "WaterMassChange[kg/s]" << ";"
                << "IntEnergyChange[J/(m^3*s)]" << ";"
                << "WaterMass[kg]" << ";"
                << "WaterMassLoss[kg]" << ";"
                << "EvaporativeFluxOverLength[kg/(m*s)]"
                << std::endl;
    }

    ~TwoPTwoCNISubProblem()
    {
        outfile.close();
    }

    /*!
     * \name Problem parameters
     */
    // \{

    //! \copydoc Dumux::ImplicitProblem::name()
    const std::string &name() const
    {
        try {
            return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Output, NamePM);
        }
        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch (...) {
            std::cerr << "Unknown exception thrown!\n";
            exit(1);
        }
    }

    //! \copydoc Dumux::ImplicitProblem::init()
    void init()
    {
        ParentType::init();
        this->model().globalStorage(storageLastTimestep_);
    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    //! \copydoc Dumux::ImplicitProblem::boundaryTypes()
    void boundaryTypes(BoundaryTypes &values, const Vertex &vertex) const
    {
        const GlobalPosition globalPos = vertex.geometry().center();
        Scalar time = this->timeManager().time();

        values.setAllNeumann();
        if (!isolatedPorousMediumBox_)
            values.setDirichlet(temperatureIdx, energyEqIdx);

        if (onUpperBoundary_(globalPos))
        {
            values.setAllNeumann();
//             values.setDirichlet(temperatureIdx, energyEqIdx);

            if (globalPos[0] > runUpDistanceX1_ + eps_
                && globalPos[0] < runUpDistanceX2_ - eps_
                && time >= initializationTime_)
            {
                values.setAllCouplingNeumann();
            }
        }
    }

    //! \copydoc Dumux::ImplicitProblem::dirichlet()
    void dirichlet(PrimaryVariables &values, const Vertex &vertex) const
    {
        const GlobalPosition globalPos = vertex.geometry().center();

        initial_(values, globalPos);
    }

    //! \copydoc Dumux::ImplicitProblem::neumann()
    void neumann(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const Intersection &is,
                 const int scvIdx,
                 const int boundaryFaceIdx) const
    {
        values = 0.;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{
    //! \copydoc Dumux::ImplicitProblem::source()
    void source(PrimaryVariables &values,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                const int scvIdx) const
    {
        values = 0.;
    }


    //! \copydoc Dumux::ImplicitProblem::initial()
    void initial(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const int scvIdx) const
    {
        const GlobalPosition &globalPos = element.geometry().corner(scvIdx);

        values = 0.;

        initial_(values, globalPos);
    }

    /*!
     * \brief Return the initial phase state inside a control volume.
     *
     * \param vertex The vertex
     * \param globalIdx The index of the global vertex
     * \param globalPos The global position
     */
    int initialPhasePresence(const Vertex &vertex,
                             const int &globalIdx,
                             const GlobalPosition &globalPos) const
    {
        return initialPhasePresenceAtPos(globalPos);
    }

    /*!
     * \brief Return the initial phase state inside a control volume.
     *
     * \param globalPos The global position
     */
    int initialPhasePresenceAtPos(const GlobalPosition &globalPos) const
    {
        return bothPhases;
    }

    /*!
     * \brief Called by the time manager after the time integration to
     *        do some post processing on the solution.
     */
    void postTimeStep()
    {
        // Calculate masses
        PrimaryVariables storage;

        this->model().globalStorage(storage);
        const Scalar time = this->timeManager().time() +  this->timeManager().timeStepSize();

        static Scalar initialWaterContent;
        if (this->timeManager().time() < 1e-6)
            initialWaterContent = storage[contiWEqIdx];

        // Write mass balance information for rank 0
        if (this->gridView().comm().rank() == 0)
        {
            if (this->timeManager().timeStepIndex() % freqMassOutput_ == 0
                || this->timeManager().episodeWillBeOver())
            {
                PrimaryVariables storageChange(0.);
                storageChange = storageLastTimestep_ - storage;

                assert(time - lastMassOutputTime_ != 0);
                storageChange /= (time - lastMassOutputTime_);

                std::cout << "Time[s]: " << time
                          << " TotalMass[kg]: " << storage[contiTotalMassIdx]
                          << " WaterMass[kg]: " << storage[contiWEqIdx]
                          << " IntEnergy[J/m^3]: " << storage[energyEqIdx]
                          << " WaterMassChange[kg/s]: " << storageChange[contiWEqIdx]
                          << std::endl;
                if (this->timeManager().time() != 0.)
                    outfile << time << ";"
                            << storageChange[contiTotalMassIdx] << ";"
                            << storageChange[contiWEqIdx] << ";"
                            << storageChange[energyEqIdx] << ";"
                            << storage[contiWEqIdx] << ";"
                            << initialWaterContent - storage[contiWEqIdx] << ";"
                            << storageChange[contiWEqIdx] / (bBoxMax_[0]-bBoxMin_[0])
                            << std::endl;

                std::cout << "eNew/eOld[-]: " << storageChange[contiWEqIdx] / storageChangeLastTimestep_[contiWEqIdx]
                          << " de[mm/d]: " << (storageChange[contiWEqIdx] - storageChangeLastTimestep_[contiWEqIdx]) / (bBoxMax_[0]-bBoxMin_[0]) *86400.0
                          << " e[mm/d]: " << storageChange[contiWEqIdx] / (bBoxMax_[0]-bBoxMin_[0]) * 86400.0
                          << std::endl;

                bool temp = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, TimeManager, AbortIfEvapRateIsConstant);
                if (storageChangeLastTimestep_[contiWEqIdx] * 1.001 > storageChange[contiWEqIdx]
                    && storageChangeLastTimestep_[contiWEqIdx] * 0.999 < storageChange[contiWEqIdx]
                    && this->timeManager().time() > 360
                    && temp)
//                     && GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, TimeManager, AbortIfEvapRateIsConstant))
                {
                    std::cout << "Evaporation rate is constant, simulation will be aborted." << std::endl;
                    std::cout << "Final steady state evaporation rate [mm/d]: " << (storageChange[contiWEqIdx] / (bBoxMax_[0]-bBoxMin_[0])) * 86400.0
                              << " velocity[m/s]: " << refVelocity_
                              << " patchSize[m]: " << (bBoxMax_[0]-bBoxMin_[0])
                              << " patchLocation[m]: " << (bBoxMax_[0]+bBoxMin_[0]) / 2.0
                              << std::endl;
                    exit(0);
                }
                storageLastTimestep_ = storage;
                storageChangeLastTimestep_ = storageChange;
                lastMassOutputTime_ = time;
            }
        }
    }

    /*!
     * \brief Determine if we are on a corner of the grid
     *
     * \param globalPos The global position
     *
     */
    bool isCornerPoint(const GlobalPosition &globalPos)
    {
        return ((onLeftBoundary_(globalPos) && onLowerBoundary_(globalPos))
                || (onLeftBoundary_(globalPos) && onUpperBoundary_(globalPos))
                || (onRightBoundary_(globalPos) && onLowerBoundary_(globalPos))
                || (onRightBoundary_(globalPos) && onUpperBoundary_(globalPos)));
    }

    /*!
     * \brief Returns whether the position is an interface corner point
     *
     * This function is required in case of mortar coupling otherwise it should return false
     *
     * \param globalPos The global position
     */
    bool isInterfaceCornerPoint(const GlobalPosition &globalPos) const
    { return false; }

    /*!
     * \brief Returns the index of the used soil type
     *
     * \param pos The global position
     */
    const unsigned checkSoilType(const GlobalPosition &globalPos) const
    {
      if (globalPos[0] < heterogeneityAtX_)
        return 1;
      else
        return 2;
    }

    // \}

private:
    /*!
     * \brief Internal method for the initial condition
     *        (reused for the dirichlet conditions!)
     */
    void initial_(PrimaryVariables &values,
                  const GlobalPosition &globalPos) const
    {
        //TODO: call density from fluidsystem
        values[pressureIdx] = refPressure_;

        if (GET_PROP_VALUE(TypeTag, Formulation) == TwoPTwoCFormulation::pnsw)
        {
            if (checkSoilType(globalPos) == 1)
                values[switchIdx] = refSw_;
            else /*checkSoilType(globalPos) == 2*/
                values[switchIdx] = refSw2_;
        }
        else
        {
            if (checkSoilType(globalPos) == 1)
                values[switchIdx] = 1.0 - refSw_;
            else /*checkSoilType(globalPos) == 2*/
                values[switchIdx] = 1.0 - refSw2_;
        }
        values[temperatureIdx] = refTemperature_;
    }

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < bBoxMin_[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > bBoxMax_[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < bBoxMin_[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > bBoxMax_[1] - eps_; }

    bool onBoundary_(const GlobalPosition &globalPos) const
    {
        return (onLeftBoundary_(globalPos) || onRightBoundary_(globalPos)
                || onLowerBoundary_(globalPos) || onUpperBoundary_(globalPos));
    }

    static constexpr Scalar eps_ = 1e-8;
    GlobalPosition bBoxMin_;
    GlobalPosition bBoxMax_;

    int freqMassOutput_;

    PrimaryVariables storageLastTimestep_;
    PrimaryVariables storageChangeLastTimestep_;
    Scalar lastMassOutputTime_;

    Scalar refTemperature_;
    Scalar refPressure_;
    Scalar refSw_;
    Scalar refSw2_;
    bool isolatedPorousMediumBox_;
    Scalar heterogeneityAtX_;

    Scalar refVelocity_;

    Scalar runUpDistanceX1_;
    Scalar runUpDistanceX2_;
    Scalar initializationTime_;
    std::ofstream outfile;
};
} //end namespace Dumux

#endif // DUMUX_TWOPTWOCNI_SUBPROBLEM_HH
