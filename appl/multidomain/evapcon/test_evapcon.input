###############################################################
# Configuration file for experiments
###############################################################
[TimeManager]
DtInitial = 5e-1 # [s]
MaxTimeStepSize = 600 # [s]
TEnd = 1800 # [s]
AbortIfEvapRateIsConstant = false

InitTime = 0 # [s] Initialization time without coupling
EpisodeLength = 720 # [s] # 14400s = 0.5d

[Grid]
Verbosity = true
Positions0 = 0.0 0.1 0.3 0.4 # [m]
Positions1 = 0.0 0.1 0.2 # [m]
Cells0 = 2 7 2
Cells1 = 3 5
Grading1 = -1.50 1.50

NoDarcyX1 = 0.10 # [m] # Beginning of PM below
NoDarcyX2 = 0.30 # [m] # End of PM below (relative to end of domain)
RunUpDistanceY = 0.1 # [m] # End of step at inflow and outflow (y-coordinate)
InterfacePosY = 0.1 # [m] # Vertical position of coupling interface

[Output]
NameFF = zeroeq2cni_reference
NamePM = 2p2cni_reference
# Frequency of restart file, flux and VTK output
FreqRestart = 500            # how often restart files are written out
FreqOutput = 5               #  10  # frequency of VTK output
FreqMassOutput = 5           #  20  # frequency of mass and evaporation rate output (Darcy)
FreqVaporFluxOutput = 3      #   5  # frequency of summarized flux output


###############################################################
# Define conditions in the two subdomains
###############################################################
[FreeFlow]
EnableNeumannInflow = false
RefVelocity = 0.5 # [m/s]
RefPressure = 1e5 # [Pa]
RefMassfrac = 0.005 # [-]
RefTemperature = 293.0 # [K]

[BoundaryLayer]
Model = 0

[PorousMedium]
IsolatedPorousMediumBox = true
RefPressurePM = 1e5 # [Pa]
RefTemperaturePM = 293.0 # [K]
RefSw = 0.8 # [-]
RefSw2 = 0.25 # [-]

[SpatialParams]
AlphaBJ = 1.0 # [-]
HeterogeneityAtX = 0.2 # [m]
LeftSoil = 1 # SILT
RightSoil = 2 # SANDY LOAM
PlotMaterialLaw = false
### EVAPCON - SILT
Permeability = 1.076e-12 # [m^2]
Porosity = 0.35 # [-]
Swr = 0.057 # [-] # WaterContentResidual / Porosity
Snr = 0.029 # [-] # 0.01 / Porosity
VgAlpha = 4.281e-5 # [1/Pa]
VgN = 1.324 # [-]
RegularizationLowSw = 0.05 # [-]
ThermalConductivitySolid = 5.9 # [W/(m*K)]
### EVAPCON - SANDY LOAM
Permeability2 = 1.252e-12 # [m^2]
Porosity2 = 0.41 # [-]
Swr2 = 0.159 # [-] # WaterContentResidual / Porosity
Snr2 = 0.024 # [-] # 0.01 / Porosity
VgAlpha2 = 8.155e-4 # [1/Pa]
VgN2 = 1.65 # [-] # not sure if this is correct
RegularizationLowSw2 = 0.05 # [-]
ThermalConductivitySolid2 = 5.9 # [W/(m*K)]

NoAdvectiveFluxOverPMInterface = false
NoDiffusiveFluxOverPMInterface = false
NoConductiveFluxOverPMInterface = false


###############################################################
# Newton and Linear solver parameters
###############################################################
[Newton]
TargetSteps = 5
MaxSteps = 8
WriteConvergence = false


###############################################################
# Eddy Viscosity Models
# 0 = none
# 1 = Prandtl
###############################################################
# Eddy Diffusivity and Eddy Conductivity Models
# 0 = none
# 1 = Reynolds analogy
###############################################################
[ZeroEq]
EddyViscosityModel = 1
EddyDiffusivityModel = 1
EddyConductivityModel = 1
BBoxMinSandGrainRoughness = 0.0 # [m]
BBoxMaxSandGrainRoughness = 0.0 # [m]
