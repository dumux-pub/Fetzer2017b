// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/**
 * \file
 * \brief The problem class for the coupling of a non-isothermal two-component ZeroEq
 *        and a non-isothermal two-phase two-component Darcy model.
 */
#ifndef DUMUX_TWOCNIZEROEQTWOPTWOCNIPROBLEM_HH
#define DUMUX_TWOCNIZEROEQTWOPTWOCNIPROBLEM_HH

#include <dune/grid/multidomaingrid.hh>
#include <dune/grid/common/gridinfo.hh>

#include <dumux/material/fluidsystems/h2oair.hh>
#include <dumux/multidomain/2cnistokes2p2cni/localoperator.hh>
#include <dumux/multidomain/2cnistokes2p2cni/problem.hh>
#include <dumux/multidomain/2cnistokes2p2cni/propertydefaults.hh>

#include "2cnizeroeq2p2cnispatialparameters.hh"
#include "2p2cnisubproblem.hh"
#include "zeroeq2cnisubproblem.hh"

namespace Dumux
{
template <class TypeTag>
class TwoCNIZeroEqTwoPTwoCNIProblem;

namespace Properties
{
NEW_TYPE_TAG(TwoCNIZeroEqTwoPTwoCNIProblem, INHERITS_FROM(TwoCNIStokesTwoPTwoCNI));

// Set the grid type
SET_TYPE_PROP(TwoCNIZeroEqTwoPTwoCNIProblem, Grid, Dune::YaspGrid<2, Dune::TensorProductCoordinates<typename GET_PROP_TYPE(TypeTag, Scalar), 2> >);

// Set the global problem
SET_TYPE_PROP(TwoCNIZeroEqTwoPTwoCNIProblem, Problem, TwoCNIZeroEqTwoPTwoCNIProblem<TypeTag>);

// Set the local coupling operator
SET_TYPE_PROP(TwoCNIZeroEqTwoPTwoCNIProblem, MultiDomainCouplingLocalOperator,
              Dumux::TwoCNIStokesTwoPTwoCNILocalOperator<TypeTag>);

// Set the two sub-problems of the global problem
SET_TYPE_PROP(TwoCNIZeroEqTwoPTwoCNIProblem, SubDomain1TypeTag, TTAG(ZeroEq2cniSubProblem));
SET_TYPE_PROP(TwoCNIZeroEqTwoPTwoCNIProblem, SubDomain2TypeTag, TTAG(TwoPTwoCNISubProblem));

// Set the global problem in the context of the two sub-problems
SET_TYPE_PROP(ZeroEq2cniSubProblem, MultiDomainTypeTag, TTAG(TwoCNIZeroEqTwoPTwoCNIProblem));
SET_TYPE_PROP(TwoPTwoCNISubProblem, MultiDomainTypeTag, TTAG(TwoCNIZeroEqTwoPTwoCNIProblem));

// Set the other sub-problem for each of the two sub-problems
SET_TYPE_PROP(ZeroEq2cniSubProblem, OtherSubDomainTypeTag, TTAG(TwoPTwoCNISubProblem));
SET_TYPE_PROP(TwoPTwoCNISubProblem, OtherSubDomainTypeTag, TTAG(ZeroEq2cniSubProblem));

// Set the same spatial parameters for both sub-problems
SET_TYPE_PROP(ZeroEq2cniSubProblem, SpatialParams, Dumux::TwoCNIZeroEqTwoPTwoCNISpatialParams<TypeTag>);

// Set the fluid system
SET_TYPE_PROP(TwoCNIZeroEqTwoPTwoCNIProblem, FluidSystem,
              FluidSystems::H2OAir<typename GET_PROP_TYPE(TypeTag, Scalar)>);

// If SuperLU is not available, the UMFPack solver is used:
#ifdef HAVE_SUPERLU
SET_TYPE_PROP(TwoCNIZeroEqTwoPTwoCNIProblem, LinearSolver, SuperLUBackend<TypeTag>);
#else
SET_TYPE_PROP(TwoCNIZeroEqTwoPTwoCNIProblem, LinearSolver, UMFPackBackend<TypeTag>);
#endif
}

/*!
 * \brief The problem class for the coupling of a non-isothermal two-component ZeroEq (zeroeq2cni)
 *        and a non-isothermal two-phase two-component Darcy model (2p2cni).
 *
 *        The problem class for the coupling of a non-isothermal two-component ZeroEq (zeroeq2cni)
 *        and a non-isothermal two-phase two-component Darcy model (2p2cni).
 *        It uses the 2p2cniCoupling model and the ZeroEq2cnicoupling model and provides
 *        the problem specifications for common parameters of the two submodels.
 *        The initial and boundary conditions of the submodels are specified in the two subproblems,
 *        2p2cnisubproblem.hh and zeroeq2cnisubproblem.hh, which are accessible via the coupled problem.
 */
template <class TypeTag = TTAG(TwoCNIZeroEqTwoPTwoCNIProblem) >
class TwoCNIZeroEqTwoPTwoCNIProblem : public TwoCNIStokesTwoPTwoCNIProblem<TypeTag>
{
    typedef TwoCNIZeroEqTwoPTwoCNIProblem<TypeTag> ThisType;
    typedef TwoCNIStokesTwoPTwoCNIProblem<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GET_PROP_TYPE(TypeTag, SubDomain1TypeTag) ZeroEq2cniTypeTag;
    typedef typename GET_PROP_TYPE(TypeTag, SubDomain2TypeTag) TwoPTwoCNITypeTag;

    typedef typename GET_PROP_TYPE(ZeroEq2cniTypeTag, Problem) ZeroEq2cniSubProblem;
    typedef typename GET_PROP_TYPE(TwoPTwoCNITypeTag, Problem) TwoPTwoCNISubProblem;

    typedef typename GET_PROP_TYPE(ZeroEq2cniTypeTag, GridView) ZeroEq2cniGridView;
    typedef typename GET_PROP_TYPE(TwoPTwoCNITypeTag, GridView) TwoPTwoCNIGridView;

    typedef typename GET_PROP_TYPE(ZeroEq2cniTypeTag, PrimaryVariables) ZeroEq2cniPrimaryVariables;
    typedef typename GET_PROP_TYPE(TwoPTwoCNITypeTag, PrimaryVariables) TwoPTwoCNIPrimaryVariables;

    typedef typename GET_PROP_TYPE(ZeroEq2cniTypeTag, ElementSolutionVector) ElementSolutionVector1;
    typedef typename GET_PROP_TYPE(TwoPTwoCNITypeTag, ElementSolutionVector) ElementSolutionVector2;

    typedef typename GET_PROP_TYPE(ZeroEq2cniTypeTag, ElementVolumeVariables) ElementVolumeVariables1;
    typedef typename GET_PROP_TYPE(TwoPTwoCNITypeTag, ElementVolumeVariables) ElementVolumeVariables2;

    typedef typename GET_PROP_TYPE(ZeroEq2cniTypeTag, FluxVariables) BoundaryVariables1;

    typedef typename GET_PROP_TYPE(ZeroEq2cniTypeTag, FVElementGeometry) FVElementGeometry1;
    typedef typename GET_PROP_TYPE(TwoPTwoCNITypeTag, FVElementGeometry) FVElementGeometry2;

    typedef typename GET_PROP_TYPE(TypeTag, Grid) HostGrid;
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGrid) MDGrid;
    typedef typename MDGrid::SubDomainGrid SDGrid;

    typedef typename MDGrid::Traits::template Codim<0>::Entity MDElement;
    typedef typename MDGrid::Traits::template Codim<0>::EntityPointer MDElementPointer;
    typedef typename ZeroEq2cniGridView::template Codim<0>::Entity SDElement1;
    typedef typename TwoPTwoCNIGridView::template Codim<0>::Entity SDElement2;
    typedef typename SDGrid::Traits::template Codim<0>::EntityPointer SDElementPointer;

    typedef typename GET_PROP_TYPE(ZeroEq2cniTypeTag, Indices) ZeroEq2cniIndices;
    typedef typename GET_PROP_TYPE(TwoPTwoCNITypeTag, Indices) TwoPTwoCNIIndices;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;

    enum { dim = ZeroEq2cniGridView::dimension };
    enum { // indices in the ZeroEq domain
        momentumXIdx1 = ZeroEq2cniIndices::momentumXIdx, // Index of the x-component of the momentum balance
        momentumYIdx1 = ZeroEq2cniIndices::momentumYIdx, // Index of the y-component of the momentum balance
        momentumZIdx1 = ZeroEq2cniIndices::momentumZIdx, // Index of the z-component of the momentum balance
        massBalanceIdx1 = ZeroEq2cniIndices::massBalanceIdx, // Index of the mass balance
        transportEqIdx1 = ZeroEq2cniIndices::transportEqIdx, // Index of the transport equation
        energyEqIdx1 = ZeroEq2cniIndices::energyEqIdx // Index of the transport equation
    };
    enum { // indices of the PVs in the Darcy domain
        massBalanceIdx2 = TwoPTwoCNIIndices::pressureIdx,
        switchIdx2 = TwoPTwoCNIIndices::switchIdx,
        temperatureIdx2 = TwoPTwoCNIIndices::temperatureIdx
    };
    enum { // indices of the balance equations
        contiTotalMassIdx2 = TwoPTwoCNIIndices::contiNEqIdx,
        contiWEqIdx2 = TwoPTwoCNIIndices::contiWEqIdx,
        energyEqIdx2 = TwoPTwoCNIIndices::energyEqIdx
    };
    enum { transportCompIdx1 = ZeroEq2cniIndices::transportCompIdx };
    enum {
        wCompIdx2 = TwoPTwoCNIIndices::wCompIdx,
        nCompIdx2 = TwoPTwoCNIIndices::nCompIdx
    };
    enum { phaseIdx = GET_PROP_VALUE(ZeroEq2cniTypeTag, PhaseIdx) };
    enum {
        numEq1 = GET_PROP_VALUE(ZeroEq2cniTypeTag, NumEq),
        numEq2 = GET_PROP_VALUE(TwoPTwoCNITypeTag, NumEq)
    };

    typedef Dune::FieldVector<Scalar, dim> GlobalPosition;
    typedef Dune::FieldVector<Scalar, dim> FieldVector;

    typedef typename MDGrid::template Codim<0>::LeafIterator ElementIterator;
    typedef typename MDGrid::LeafSubDomainInterfaceIterator SDInterfaceIterator;

public:
    /*!
     * \brief The problem for the coupling of the free-flow transport and Darcy flow
     *
     * \param timeManager The time manager
     * \param gridView The grid view
     */
    template<class GridView>
    TwoCNIZeroEqTwoPTwoCNIProblem(TimeManager &timeManager,
                                  GridView gridView)
    : ParentType(timeManager, gridView),
      eps_(1e-7)
    {
        // define location of the interface
        interfacePos_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePosY);
        noDarcyX1_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX1);
        noDarcyX2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX2);
        heterogeneityAtX_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, HeterogeneityAtX);
        episodeLength_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, EpisodeLength);
        initializationTime_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, InitTime);
        dtInit_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, DtInitial);

        // define output options
        freqRestart_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Output, FreqRestart);
        freqOutput_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Output, FreqOutput);
        freqVaporFluxOutput_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Output, FreqVaporFluxOutput);

        zeroeq2cni_ = this->sdID1();
        twoPtwoCNI_ = this->sdID2();

        initializeGrid();

        // initialize the tables of the fluid system
        FluidSystem::init(/*tempMin=*/273.15, /*tempMax=*/343.15, /*numTemp=*/140,
                          /*pMin=*/5e4, /*pMax=*/1e7, /*numP=*/995);

        if (initializationTime_ > 0.0)
            this->timeManager().startNextEpisode(initializationTime_);
        else
            this->timeManager().startNextEpisode(episodeLength_);

        this->sdProblem2().spatialParams().plotMaterialLaw();

        fluxFile_.open("fluxes.out");
        fluxFile_ << "Time[s];"
                  << "TotalWaterVaporFluxFF[kg/(m*s)];EvapRate[mm/d];AdvWaterVaporFluxFF[kg/(m*s)];DiffWaterVaporFluxFF[kg/(m*s)];EnergyFluxFF;"
                  << "TotalWaterVaporFluxFF_1[kg/(m*s)];EvapRate_1[mm/d];AdvWaterVaporFluxFF_1[kg/(m*s)];DiffWaterVaporFluxFF_1[kg/(m*s)];EnergyFluxFF_1;"
                  << "TotalWaterVaporFluxFF_2[kg/(m*s)];EvapRate_2[mm/d];AdvWaterVaporFluxFF_2[kg/(m*s)];DiffWaterVaporFluxFF_2[kg/(m*s)];EnergyFluxFF_2;"
                  << std::endl;
    }

    ~TwoCNIZeroEqTwoPTwoCNIProblem()
    {
        fluxFile_.close();
    }

    /*!
     * \brief docme
     */
    void initializeGrid()
    {
        MDGrid& mdGrid = this->mdGrid();
        mdGrid.startSubDomainMarking();

        // subdivide grid in two subdomains
        ElementIterator eendit = mdGrid.template leafend<0>();
        for (ElementIterator elementIt = mdGrid.template leafbegin<0>();
             elementIt != eendit; ++elementIt)
        {
            // this is required for parallelization
            // checks if element is within a partition
            if (elementIt->partitionType() != Dune::InteriorEntity)
                continue;

            GlobalPosition globalPos = elementIt->geometry().center();

            if (globalPos[1] > interfacePos_)
                mdGrid.addToSubDomain(zeroeq2cni_,*elementIt);
            else
                if(globalPos[0] > noDarcyX1_ && globalPos[0] < noDarcyX2_)
                    mdGrid.addToSubDomain(twoPtwoCNI_,*elementIt);
        }
        mdGrid.preUpdateSubDomains();
        mdGrid.updateSubDomains();
        mdGrid.postUpdateSubDomains();

        gridinfo(this->sdGrid1());
        gridinfo(this->sdGrid2());
    }

    //! \copydoc Dumux::CoupledProblem::postTimeStep()
    void postTimeStep()
    {
        // call the postTimeStep function of the subproblems
        this->sdProblem1().postTimeStep();
        this->sdProblem2().postTimeStep();

        if (shouldWriteVaporFlux())
        {
            calculateFirstInterfaceFluxes();
//            calculateSecondInterfaceFluxes();
        }
    }

    //! \copydoc Dumux::CoupledProblem::episodeEnd()
    void episodeEnd()
    { this->timeManager().startNextEpisode(episodeLength_); }

    /*!
     * \brief Calculates fluxes and coupling terms at the interface for the ZeroEq model.
     *        Flux output files are created and the summarized flux is written to a file.
     */
    void calculateFirstInterfaceFluxes()
    {
        const MDGrid& mdGrid = this->mdGrid();
        ElementVolumeVariables1 elemVolVarsPrev1, elemVolVarsCur1;
        FVElementGeometry1 fvGeometry1;
        Scalar totalWaterVaporFlux = 0.;
        Scalar advectiveWaterVaporFlux = 0.;
        Scalar diffusiveWaterVaporFlux = 0.;
        Scalar energyFlux = 0.;
        Scalar area = 0.;
        Scalar totalWaterVaporFlux_1 = 0.;
        Scalar advectiveWaterVaporFlux_1 = 0.;
        Scalar diffusiveWaterVaporFlux_1 = 0.;
        Scalar energyFlux_1 = 0.;
        Scalar area_1 = 0.;
        Scalar totalWaterVaporFlux_2 = 0.;
        Scalar advectiveWaterVaporFlux_2 = 0.;
        Scalar diffusiveWaterVaporFlux_2 = 0.;
        Scalar energyFlux_2 = 0.;
        Scalar area_2 = 0.;

        // loop over the element faces on the interface
        const SDInterfaceIterator endIfIt = mdGrid.leafSubDomainInterfaceEnd(zeroeq2cni_, twoPtwoCNI_);
        for (SDInterfaceIterator ifIt =
                 mdGrid.leafSubDomainInterfaceBegin(zeroeq2cni_, twoPtwoCNI_); ifIt != endIfIt; ++ifIt)
        {
            const int firstFaceIdx = ifIt->indexInFirstCell();
            const std::shared_ptr<MDElement> mdElement1
                = std::make_shared<MDElement>(ifIt->firstCell());
            const std::shared_ptr<SDElement1> sdElement1
                = std::make_shared<SDElement1>(this->sdElementPointer1(*mdElement1));
            fvGeometry1.update(this->sdGridView1(), *sdElement1);

            const Dune::ReferenceElement<typename MDGrid::ctype,dim>& referenceElement1 =
                Dune::ReferenceElements<typename MDGrid::ctype,dim>::general((*mdElement1).type());
            const int numVerticesOfFace = referenceElement1.size(firstFaceIdx, 1, dim);

            elemVolVarsPrev1.update(this->sdProblem1(),
                                    *sdElement1,
                                    fvGeometry1,
                                    true /* oldSol? */);
            elemVolVarsCur1.update(this->sdProblem1(),
                                   *sdElement1,
                                   fvGeometry1,
                                   false /* oldSol? */);

            typedef typename GET_PROP_TYPE(ZeroEq2cniTypeTag, ElementBoundaryTypes) ElementBoundaryTypes;
            ElementBoundaryTypes bcTypes;
            bcTypes.update(this->sdProblem1(), *sdElement1, fvGeometry1);
            this->localResidual1().evalPDELab(*sdElement1, fvGeometry1, elemVolVarsPrev1, elemVolVarsCur1, bcTypes);

            for (int nodeInFace = 0; nodeInFace < numVerticesOfFace; nodeInFace++)
            {
                int boundaryFaceIdx =
                    fvGeometry1.boundaryFaceIndex(firstFaceIdx, nodeInFace);
                const int vertInElem1 = referenceElement1.subEntity(firstFaceIdx, 1, nodeInFace, dim);
                const FieldVector& vertexGlobal = (*mdElement1).geometry().corner(vertInElem1);
                const ElementSolutionVector1& firstVertexResidual = this->localResidual1().residual();

                BoundaryVariables1 boundaryVars1;
                boundaryVars1.update(this->sdProblem1(),
                                     *sdElement1,
                                     fvGeometry1,
                                     boundaryFaceIdx,
                                     elemVolVarsCur1,
                                     /*onBoundary=*/true);

                const Scalar scvFaceArea = boundaryVars1.face().area;
                totalWaterVaporFlux += firstVertexResidual[vertInElem1][transportEqIdx1];
                advectiveWaterVaporFlux += computeAdvectiveVaporFluxes1(elemVolVarsCur1, boundaryVars1, vertInElem1);
                diffusiveWaterVaporFlux += computeDiffusiveVaporFluxes1(elemVolVarsCur1, boundaryVars1, vertInElem1);
                energyFlux += firstVertexResidual[vertInElem1][energyEqIdx1];
                area += scvFaceArea;

                Scalar weight = 1.0;
                if (checkSoilType(vertexGlobal) == 3)
                    weight = 0.5;

                if (checkSoilType(vertexGlobal) == 1)
                {
                    totalWaterVaporFlux_1 += weight * firstVertexResidual[vertInElem1][transportEqIdx1];
                    advectiveWaterVaporFlux_1 += weight * computeAdvectiveVaporFluxes1(elemVolVarsCur1, boundaryVars1, vertInElem1);
                    diffusiveWaterVaporFlux_1 += weight * computeDiffusiveVaporFluxes1(elemVolVarsCur1, boundaryVars1, vertInElem1);
                    energyFlux_1 += weight * firstVertexResidual[vertInElem1][energyEqIdx1];
                    area_1 += weight * scvFaceArea;
                }
                else
                {
                    totalWaterVaporFlux_2 += weight * firstVertexResidual[vertInElem1][transportEqIdx1];
                    advectiveWaterVaporFlux_2 += weight * computeAdvectiveVaporFluxes1(elemVolVarsCur1, boundaryVars1, vertInElem1);
                    diffusiveWaterVaporFlux_2 += weight * computeDiffusiveVaporFluxes1(elemVolVarsCur1, boundaryVars1, vertInElem1);
                    energyFlux_2 += weight * firstVertexResidual[vertInElem1][energyEqIdx1];
                    area_2 += weight * scvFaceArea;
                }
            }
        } // end loop over element faces on interface

        fluxFile_ << this->timeManager().time() + this->timeManager().timeStepSize() << ";"
                  << totalWaterVaporFlux << ";"
                  << totalWaterVaporFlux * 86400.0 / area << ";"
                  << advectiveWaterVaporFlux << ";"
                  << diffusiveWaterVaporFlux << ";"
                  << energyFlux << ";"
                  << totalWaterVaporFlux_1 << ";"
                  << totalWaterVaporFlux_1 * 86400.0 / area_1 << ";"
                  << advectiveWaterVaporFlux_1 << ";"
                  << diffusiveWaterVaporFlux_1 << ";"
                  << energyFlux_1 << ";"
                  << totalWaterVaporFlux_2 << ";"
                  << totalWaterVaporFlux_2 * 86400.0 / area_2 << ";"
                  << advectiveWaterVaporFlux_2 << ";"
                  << diffusiveWaterVaporFlux_2 << ";"
                  << energyFlux_2 << ";"
                  << std::endl;

        std::cout << "EvapRate -"
                  << " Areas: (" << area << "," << area_1 << "," << area_2 << ")"
                  << " Total: " << totalWaterVaporFlux * 86400.0 / area
                  << " Soil1: " << totalWaterVaporFlux_1 * 86400.0 / area_1
                  << " Soil2: " << totalWaterVaporFlux_2 * 86400.0 / area_2
                  << std::endl;

        std::cout << "Writing fluxes.out" << std::endl;
    }

    /*!
     * \brief Calculates fluxes and coupling terms at the interface for the Darcy model.
     *        Flux output files are created and the summarized flux is written to a file.
     */
    void calculateSecondInterfaceFluxes()
    {
        std::cerr << "Implement the division by area first." << std::endl;
        exit(1);
        const MDGrid& mdGrid = this->mdGrid();
        ElementVolumeVariables2 elemVolVarsPrev2, elemVolVarsCur2;
        FVElementGeometry2 fvGeometry2;
        Scalar totalWaterComponentFlux = 0.;
        Scalar energyFlux = 0.;
        Scalar totalWaterComponentFlux_1 = 0.;
        Scalar energyFlux_1 = 0.;
        Scalar totalWaterComponentFlux_2 = 0.;
        Scalar energyFlux_2 = 0.;

        // loop over the element faces on the interface
        const SDInterfaceIterator endIfIt = mdGrid.leafSubDomainInterfaceEnd(zeroeq2cni_, twoPtwoCNI_);
        for (SDInterfaceIterator ifIt =
                 mdGrid.leafSubDomainInterfaceBegin(zeroeq2cni_, twoPtwoCNI_); ifIt != endIfIt; ++ifIt)
        {
            const int secondFaceIdx = ifIt->indexInSecondCell();
            const std::shared_ptr<MDElement> mdElement2
                = std::make_shared<MDElement>(ifIt->secondCell());
            const std::shared_ptr<SDElement2> sdElement2
                = std::make_shared<SDElement2>(this->sdElementPointer2(*mdElement2));
            fvGeometry2.update(this->sdGridView2(), *sdElement2);


            const Dune::ReferenceElement<typename MDGrid::ctype,dim>& referenceElement2 =
                Dune::ReferenceElements<typename MDGrid::ctype,dim>::general((*mdElement2).type());
            const int numVerticesOfFace = referenceElement2.size(secondFaceIdx, 1, dim);

            elemVolVarsPrev2.update(this->sdProblem2(),
                                    *sdElement2,
                                    fvGeometry2,
                                    true /* oldSol? */);
            elemVolVarsCur2.update(this->sdProblem2(),
                                   *sdElement2,
                                   fvGeometry2,
                                   false /* oldSol? */);

            typedef typename GET_PROP_TYPE(TwoPTwoCNITypeTag, ElementBoundaryTypes) ElementBoundaryTypes;
            ElementBoundaryTypes bcTypes;
            bcTypes.update(this->sdProblem2(), *sdElement2, fvGeometry2);

            this->localResidual2().evalPDELab(*sdElement2, fvGeometry2, elemVolVarsPrev2, elemVolVarsCur2, bcTypes);

            for (int nodeInFace = 0; nodeInFace < numVerticesOfFace; nodeInFace++)
            {
                const int vertInElem2 = referenceElement2.subEntity(secondFaceIdx, 1, nodeInFace, dim);
                const FieldVector& vertexGlobal = mdElement2.geometry().corner(vertInElem2);
                const ElementSolutionVector2& secondVertexResidual = this->localResidual2().residual();

                totalWaterComponentFlux += secondVertexResidual[vertInElem2][contiWEqIdx2];
                energyFlux += secondVertexResidual[vertInElem2][energyEqIdx2];

                if (checkSoilType(vertexGlobal) == 1)
                {
                    totalWaterComponentFlux_1 += secondVertexResidual[vertInElem2][contiWEqIdx2];
                    energyFlux_1 += secondVertexResidual[vertInElem2][energyEqIdx2];
                }
                else /*checkSoilType(vertexGlobal) == 2 od. 3*/
                {
                    totalWaterComponentFlux_2 += secondVertexResidual[vertInElem2][contiWEqIdx2];
                    energyFlux_2 += secondVertexResidual[vertInElem2][energyEqIdx2];
                }
            }
        }

        fluxFile_ << totalWaterComponentFlux << ";"
                  << energyFlux << ";"
                  << totalWaterComponentFlux_1 << ";"
                  << energyFlux_1 << ";"
                  << totalWaterComponentFlux_2 << ";"
                  << energyFlux_2 << ";"
                  << std::endl;
    }

    Scalar computeAdvectiveVaporFluxes1(const ElementVolumeVariables1& elemVolVars1,
                                        const BoundaryVariables1& boundaryVars1,
                                        int vertInElem1)
    {
        return elemVolVars1[vertInElem1].density()
               * elemVolVars1[vertInElem1].fluidState().massFraction(phaseIdx, transportCompIdx1)
               * boundaryVars1.normalVelocity();;
    }

    Scalar computeDiffusiveVaporFluxes1(const ElementVolumeVariables1& elemVolVars1,
                                        const BoundaryVariables1& boundaryVars1,
                                        int vertInElem1)
    {
        return (boundaryVars1.moleFractionGrad(transportCompIdx1)
                * boundaryVars1.face().normal)
                * (boundaryVars1.diffusionCoeff(transportCompIdx1)
                   + boundaryVars1.eddyDiffusivity())
                * boundaryVars1.molarDensity()
                * FluidSystem::molarMass(transportCompIdx1);
    }

    //! \copydoc Dumux::CoupledProblem::shouldWriteRestartFile()
    bool shouldWriteRestartFile() const
    {
        return ((this->timeManager().timeStepIndex() > 0 &&
                (this->timeManager().timeStepIndex() % freqRestart_ == 0))
                // also write a restart file at the end of each episode
                || this->timeManager().episodeWillBeOver());
    }


    //! \copydoc Dumux::CoupledProblem::shouldWriteOutput()
    bool shouldWriteOutput() const
    {
        return (this->timeManager().timeStepIndex() % freqOutput_ == 0
                || this->timeManager().episodeWillBeOver());
    }

    /*!
     * \brief Returns true if the summarized vapor fluxes
     *        across the free-flow -- porous-medium interface,
     *        representing the evaporation rate (related to the
     *        interface area), should be written.
     */
    bool shouldWriteVaporFlux() const
    {
        return (this->timeManager().timeStepIndex() % freqVaporFluxOutput_ == 0
                || this->timeManager().episodeWillBeOver());
    }

    /*!
     * \brief Returns the index of the used soil type
     *
     * \param pos The global position
     */
    const unsigned checkSoilType(const GlobalPosition &globalPos) const
    {
        if (globalPos[0] < heterogeneityAtX_ - eps_)
            return 1;
        else if  (globalPos[0] > heterogeneityAtX_ + eps_)
            return 2;
        else // on interface between
            return 3;
    }

    ZeroEq2cniSubProblem& zeroeq2cniProblem()
    { return this->sdProblem1(); }
    const ZeroEq2cniSubProblem& zeroeq2cniProblem() const
    { return this->sdProblem1(); }

    TwoPTwoCNISubProblem& twoPtwoCNIProblem()
    { return this->sdProblem2(); }
    const TwoPTwoCNISubProblem& twoPtwoCNIProblem() const
    { return this->sdProblem2(); }

private:
    typename MDGrid::SubDomainType zeroeq2cni_;
    typename MDGrid::SubDomainType twoPtwoCNI_;

    unsigned freqRestart_;
    unsigned freqOutput_;
    unsigned freqVaporFluxOutput_;

    Scalar eps_;
    Scalar interfacePos_;
    Scalar noDarcyX1_;
    Scalar noDarcyX2_;
    Scalar heterogeneityAtX_;
    Scalar episodeLength_;
    Scalar initializationTime_;
    Scalar dtInit_;

    std::ofstream fluxFile_;
};

} //end namespace

#endif // DUMUX_TWOCNIZEROEQTWOPTWOCNIPROBLEM_HH
