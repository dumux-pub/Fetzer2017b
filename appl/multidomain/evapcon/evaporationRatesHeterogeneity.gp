reset
set datafile separator ';'

set terminal pngcairo size 800,600
set output 'evaporationRatesHeterogeneity.png'
set xlabel 'Time [d]'
set ylabel 'Evaporation rate [mm/d]'
set yrange [0:20]

### ATTENTION
# a high resolution is needed, that the evapRate from storage.out can be matched
# with the evapRate from fluxes.out, because this is calculated based on gradients

plot \
'storage.out' u ($1/86400):($7*86400) w lp t 'water mass change in PM', \
'fluxes.out' u ($1/86400):3 w l,\
'fluxes.out' u ($1/86400):8 w l,\
'fluxes.out' u ($1/86400):13 w l,\
'fluxes.out' u ($1/86400):(($8+$13)/0.2) w l
