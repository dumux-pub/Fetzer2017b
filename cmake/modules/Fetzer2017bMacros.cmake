# additional macros
include(AddGnuplotFileLinks)
include(AddInputFileLinksMacro)

# define empty macros for Dune 2.3 users
if(("${DUNE_COMMON_VERSION_MAJOR}" STREQUAL "2")
    AND ("${DUNE_COMMON_VERSION_MINOR}" STREQUAL "3"))

  macro(dune_enable_all_packages)
  endmacro()

endif()
